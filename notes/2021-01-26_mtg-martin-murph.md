---
title: "Meeting notes"
date: "2021-01-26"
---



# Notes
- In attendance: Martin, Murph, Maia
- Daniel asked Martin to do some specificity checks for different PubMed queries.
- We want to focus on human research. There is already an animal search query.
- Since we are going to manually code many papers, we can build a classifier. This means more up-front time investment to design the study and decision rules for data extraction.

# Action Items
- [x] [Martin] set up https://codeberg.org/ and let Murph/Maia know username
- [x] [Maia] Add Martin to previous project repo
- [ ] [Murph] Add Martin to current project repo
- [ ] [Martin] Email CAMARADES (Sarah) and automated-research group (Tracey) to find out what work they're doing on human research classifiers (Pub)
- [ ] [Martin] Talk to Daniel to limit scope to human research and expand to more rigorous methods
- [ ] [Martin] Set up Team channel which we can use for chatting, with notes and data in Codeberg