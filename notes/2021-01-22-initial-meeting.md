---
title: "Meeting notes"
date: "2021-01-22"
---

# Notes
* In attendance: Maia and Murph
* [Notes from previous project](https://codeberg.org/maia-sh/pm_ct_evaluation/src/branch/master/notes/rolling-minutes.md)
* General project outline
  * Query Pubmed for a big set that may potentially contain human research reports
  * Iteratively develop a set of variables that will allow human coders to classify reports into different classifications of human research (e.g. "human research," "clinical study," "clinical trial," "randomized controlled trial," etc.)
  * Code the big set of reports according these variables
  * Based on the reports' Pubmed Mesh terms, write an R script that determines the optimal search strategy for 1. sensitivity and 2. precision for the classifications above using half of the manually-coded data set
  * Test the search strategy on the other ("confirmation") half of the data set
* Folks we should touch base with

  * Automated screening group (Tracey)
  * NIH NLM
  * Biomedical research librarians
    * I (Maia) have been in touch with [Eva Jurczyk](https://onesearch.library.utoronto.ca/library-staff/24629/eva-jurczyk) from University of Toronto about her work on O’Leary, J.D., Crawford, M.W., Jurczyk, E. *et al.* Benchmarking bibliometrics in biomedical research: research performance of the University of Toronto’s Faculty of Medicine, 2008–2012. *Scientometrics* **105,** 311–321 (2015). https://doi.org/10.1007/s11192-015-1676-5. We could reach out to her and ask about clinical trial classification.
    * I collaborate with [Patricia Ayala](https://gerstein.library.utoronto.ca/staff/patricia-ayala) on another project and have asked her about clinical trial classification previously, and she didn't know of any validated strategies
    * I contacted the Charité library a while back requesting research librarian support. Sadly, doesn't seem available.
* Here's my previous communication with NLM [#CAS-552130-T5D6Q8: "clinical trial"[PT] sensitivity and specificity]

**My request**

> Case Created: 2020-06-10T08:03:39Z
>
> Summary: "clinical trial"[PT] sensitivity and specificity
>
> Details: Dear NLM Support Team,
>
> I am inquiring about the sensitivity and specificity of the "clinical trial"[PT] MeSH term/XML tag.
>
> How does NLM assign this term? Is it automated from meta-data from the publisher submission and/or from the publication abstract/title? Is it manual? Could you please point me to any further documentation on the methods behind this tag?
>
> Has NLM evaluated the sensitivity and specificity of the term? I.e., how many CT does this term not label as such and how many non-CT does it mislabel as CT? Could you please point me to any NLM reports/analyses, and/or academic literature, on this term's sensitivity and specificity?
>
> Many thanks,
>
> Maia

**NLM Response**

> Your INTERNET communication sent to NLM Customer Service was forwarded to the Quality Assurance Unit, Index Section, for action.
> You wrote concerning the clinical trial [PT] and how it is assigned in PubMed/MEDLINE.
>
> There are several ways the publication type can be assigned. The publisher has the option of assigning Clinical Trial [PT] or its more specific term. The indexer can also assign the PT and has the final authority. ==The sensitivity and specificity has not been assessed. However our indexers are well trained as to when to assign the correct PT so we are confident in our final product.==
>
> I hope you find this information helpful, and thank you for your interest in the completeness of the NLM databases.
>
> Annemieke van der Sluijs
>
> Index Section, Unit Head D

# Action Items

- [ ] [Murph] write a 1-page project proposal
- [x] [Maia] Look for Cochrane RCT filters
  - https://work.cochrane.org/pubmed
- [x] [Maia] Start group library in Zotero
  - https://www.zotero.org/groups/2734979/human_research_classification
- [x] [Maia] Check contacts for NLM folk
  - Did a quick search in my DC contacts and no one is popping up
  - I've mostly corresponded with NLM through the support email (see above)
  - I did once get forwarded to a specialist so I have a real person's email we could try: Mork, James (NIH/NLM/LHC) [E] <jmork@mail.nih.gov>
